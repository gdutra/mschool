# -*- encoding: utf-8 -*-
Mschool::Application.routes.draw do
  root :to => 'home#index'


  get "report/students", :to => "report#show" , :as => :report
  get "report/students/generate", :to => "report#generate" , :as => :report_generate

  scope(:path_names => { :new => "novo",
                         :edit => "editar"}) do

    resources :students, :path => "alunos"
    resources :teachers, :path => "professores"
    resources :groups, :path => "turmas"
  end




end
