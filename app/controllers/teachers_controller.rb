class TeachersController < ApplicationController


  def index
    @teachers = Teacher.all
  end

  def new
    @teacher = Teacher.new
  end


  def edit
    @teacher = Teacher.find(params[:id])
  end

  def update
    @teacher = Teacher.find(params[:id])
    if @teacher.update_attributes(params[:teacher])
      redirect_to teachers_path, :notice => "Professor editado com sucesso!"
    else
      render :edit
    end
  end

  def create
    @teacher = Teacher.new(params[:teacher])

    if @teacher.save
      flash[:notice] = "Professor criado com sucesso!"
      redirect_to :action => :index
    else
      render :new
    end
  end

  def destroy
    @teacher = Teacher.find(params[:id])
    @teacher.delete
    redirect_to teachers_path, :notice => "Professor deletado com sucesso!"
  end

end
