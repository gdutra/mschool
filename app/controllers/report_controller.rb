class ReportController < ApplicationController
  def show

  end

  def generate
    students_by_group = Student.students_grouped_by_group

    teacher_name = params[:filter_teacher] || ""
    group_name = params[:filter_group] || ""

    if is_to_filter_by_group_name(group_name)
      students_by_group = Student.students_by_group_name(group_name)
    end

    if is_to_filter_by_teacher_name(teacher_name)
      students_by_group = students_with_filtered_teacher(teacher_name)
    end

    removing_filter_params

    @report = students_by_group

    render :show
  end

  def students_with_filtered_teacher(teacher_name)
    teacher_ids = Teacher.teacher_ids_by_name(teacher_name)
    group_ids = Group.group_by_teacher_ids(teacher_ids)
    Student.students_by_group_ids(group_ids)
  end


  def removing_filter_params
    params[:filter_teacher] = nil;
    params[:filter_group] = nil;
  end

  def is_to_filter_by_group_name(group_name)
    group_name.present?
  end

  def is_to_filter_by_teacher_name(teacher_name)
    teacher_name.present?
  end
end
