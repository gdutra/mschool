class GroupsController < ApplicationController

  def index
    @groups = Group.all
  end


  def new
    @group = Group.new
    @subjects = Subject.new
  end

  def create
    @group = Group.new(params[:group])

    if @group.save
      flash[:notice] = "Turma criada com sucesso!"
      redirect_to :action => :index
    else
      render :new
    end
  end

  def destroy
    @group = Group.find(params[:id])
    @group.delete
    redirect_to groups_path, :notice => "Turma deletada com sucesso!"
  end

  def edit
    @group = Group.find(params[:id])
  end

  def update
    @group = Group.find(params[:id])
    if @group.update_attributes(params[:group])
      redirect_to groups_path, :notice => "Turma editada com sucesso!"
    else
      render :edit
    end
  end


end
