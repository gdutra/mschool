class StudentsController < ApplicationController

  def new
    @student = Student.new
  end

  def index
    @students = Student.all
  end

  def create
    @student = Student.new(params[:student])

    if @student.save
      flash[:notice] = "Aluno criado com sucesso!"
      redirect_to :action => :index
    else
      render :new
    end
  end

  def destroy
    @student = Student.find(params[:id])
    @student.delete
    redirect_to students_path, :notice => "Aluno deletado com sucesso!"
  end

  def edit
    @student = Student.find(params[:id])
  end

  def update
    @student = Student.find(params[:id])
    if @student.update_attributes(params[:student])
      redirect_to students_path, :notice => "Aluno editado com sucesso!"
    else
      render :edit
    end
  end

end
