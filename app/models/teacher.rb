# -*- encoding: utf-8 -*-
class Teacher < ActiveRecord::Base
  attr_accessible :name

  validates_presence_of :name, :message => "Nome é obrigatório!"

  has_one :subject


  def self.teacher_ids_by_name(teacher_name)
    select(:id).where("name = ?", teacher_name)
  end


end
