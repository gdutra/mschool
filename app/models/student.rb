# -*- encoding: utf-8 -*-
class Student < ActiveRecord::Base
  attr_accessible :name, :registration, :group_id

  validates_presence_of :name, :message => "Nome é obrigatório"
  validates_presence_of :registration, :message => "Matrícula é obrigatório"

  validates_numericality_of :registration, :message => "Matrícula não é um número", :only_integer => true

  belongs_to :group


  def self.students_grouped_by_group
    includes(:group)
    .all.group_by {|t| t.group.name if t.group}
  end

  def self.students_by_group_name(group_name)
    includes(:group)
    .where("groups.name = ?", group_name)
    .all.group_by {|t| t.group.name}
  end

  def self.students_by_group_ids(group_ids)
    Student.includes(:group)
    .where("groups.id in (?)", group_ids)
    .all.group_by {|t| t.group.name}
  end
end
