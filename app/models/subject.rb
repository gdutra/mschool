class Subject < ActiveRecord::Base
  attr_accessible :group_id, :name, :teacher_id

  belongs_to :group
  belongs_to :teacher
end
