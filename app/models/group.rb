class Group < ActiveRecord::Base
  attr_accessible :name, :subjects_attributes

  has_many :subjects
  has_many :students

  accepts_nested_attributes_for :subjects, :allow_destroy => true


  def self.group_by_teacher_ids(teacher_ids)
    select("groups.id").includes(:subjects)
    .where("subjects.teacher_id in (?)", teacher_ids)
  end

end
