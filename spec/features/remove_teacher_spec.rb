# -*- encoding: utf-8 -*-
require "spec_helper"


describe "Remove Teacher" do

  context "deleting a single record" do

    name = "João"

    before do
      visit root_path

      click_link "Professores"
      click_link "Cadastrar Professor"

      fill_in "Nome", :with => name

      click_button "Salvar"


      click_link "Remover"
    end

    it "removes the record" do
      expect(page).to have_no_content(name)
    end

    it "redirects to the index teachers" do
      expect(current_path).to eql(teachers_path)
    end

    it "displays a success message if deleted" do
      expect(page).to have_content("Professor deletado com sucesso!")
    end

  end
end
