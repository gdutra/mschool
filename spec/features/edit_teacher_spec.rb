# -*- encoding: utf-8 -*-
require "spec_helper"

describe "Teacher" do
  before do
    visit root_path

    click_link "Professores"
      click_link "Cadastrar Professor"

    fill_in "Nome", :with => "João"

    click_button "Salvar"
  end


  context "editing with valid data" do
    before do
      click_link "Editar"

      fill_in "Nome", :with => "José"

      click_button "Salvar"
    end

    it "changes the teacher attributes" do
      expect(page).to have_no_content("João")
    end

    it "displays a success message" do
      expect(page).to have_content("Professor editado com sucesso!")
    end

  end

  context "editing with invalid data" do
    before do
      click_link "Editar"
    end

    it "displays error message when empty data" do
      fill_in "Nome", :with => ""
      click_button "Salvar"

      expect(page).to have_content "Nome é obrigatório"
    end

  end
end
