# -*- encoding: utf-8 -*-
require "spec_helper"


describe "New Student" do
  context "with valid data" do

    before do
      visit root_path

      group = FactoryGirl.create(:group)

      click_link "Alunos"
      click_link "Cadastrar Aluno"

      fill_in "Nome", :with => "João"
      fill_in "Matrícula", :with => "2223324242"

      select(group.name, :from => 'Turma')

      click_button "Salvar"
    end

    it "displays a success message on create" do
      expect(page).to have_content("Aluno criado com sucesso!")
    end
    it "redirects to the students index" do
      expect(current_path).to eql(students_path)
    end
  end

  context "creating with invalid data" do
    before do
      visit root_path
      click_link "Alunos"
      click_link "Cadastrar Aluno"
    end

    it "displays error message when empty data" do
      fill_in "Nome", :with => ""
      fill_in "Matrícula", :with => ""
      click_button "Salvar"

      expect(page).to have_content "Matrícula é obrigatório"
      expect(page).to have_content "Nome é obrigatório"
    end

    it "displays registration error message when is not a number" do
      fill_in "Nome", :with => "João"
      fill_in "Matrícula", :with => "string"
      click_button "Salvar"

      expect(page).to have_content "Matrícula não é um número"
    end
  end
end
