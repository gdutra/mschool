# -*- encoding: utf-8 -*-
require "spec_helper"

describe "Student" do
  before do
    visit root_path

    click_link "Alunos"
    click_link "Cadastrar Aluno"

    fill_in "Nome", :with => "João"
    fill_in "Matrícula", :with => "2223324242"

    click_button "Salvar"
  end


  context "editing with valid data" do
    before do
      click_link "Editar"

      fill_in "Nome", :with => "José"
      fill_in "Matrícula", :with => "111"

      click_button "Salvar"
    end

    it "changes the student attributes" do
      expect(page).to have_no_content("João")
      expect(page).to have_no_content("2223324242")
    end

    it "displays a success message" do
      expect(page).to have_content("Aluno editado com sucesso!")
    end

  end

  context "editing with invalid data" do
    before do
      click_link "Editar"
    end

    it "displays error message when empty data" do
      fill_in "Nome", :with => ""
      fill_in "Matrícula", :with => ""
      click_button "Salvar"

      expect(page).to have_content "Matrícula é obrigatório"
      expect(page).to have_content "Nome é obrigatório"
    end

    it "displays registration error message when is not a number" do
      fill_in "Nome", :with => "João"
      fill_in "Matrícula", :with => "string"
      click_button "Salvar"

      expect(page).to have_content "Matrícula não é um número"
    end
  end
end
