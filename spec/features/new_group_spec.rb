# -*- encoding: utf-8 -*-
require "spec_helper"

describe "Group" do
  context "with valid data" do
    before do
      visit root_path

      teacher_1 = FactoryGirl.create(:teacher_1)
      teacher_2 = FactoryGirl.create(:teacher_2)

      click_link "Turmas"
      click_link "Cadastrar Turma"

      fill_in "Nome da Turma", :with => "1º Ano"

      click_link("Adcionar matéria")

    end

    it "displays a success message on create" do

    end

    it "redirects to the groups index" do

    end

  end

  context "with invalid data" do

  end
end
