# -*- encoding: utf-8 -*-
require "spec_helper"

describe "New Teacher" do
  context "with valid data" do
    before do
      visit root_path

      click_link "Professores"
      click_link "Cadastrar Professor"

      fill_in "Nome", :with => "Roberto"

      click_button "Salvar"
    end

    it "displays a success message on create" do
      expect(page).to have_content("Professor criado com sucesso!")
    end

    it "redirects to the teachers index" do
      expect(current_path).to eql(teachers_path)
    end

  end

  context "with invalid data" do
    before do
      visit root_path
      click_link "Professores"
      click_link "Cadastrar Professor"
    end

    it "displays error message when empty data" do
      fill_in "Nome", :with => ""
      click_button "Salvar"

      expect(page).to have_content "Nome é obrigatório"
    end
  end
end
