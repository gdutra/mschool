# -*- encoding: utf-8 -*-
require "spec_helper"


describe "Remove Student" do

  context "deleting a single record" do

    name = "João"
    registration = "2223324242"

    before do
      visit root_path

      click_link "Alunos"
      click_link "Cadastrar Aluno"


      fill_in "Nome", :with => name
      fill_in "Matrícula", :with => registration

      click_button "Salvar"

      click_link "Remover"
    end

    it "removes the record" do
      expect(page).to have_no_content(name)
      expect(page).to have_no_content(registration)
    end

    it "redirects to the index students" do
      expect(current_path).to eql(students_path)
    end

    it "displays a success message if deleted" do
      expect(page).to have_content("Aluno deletado com sucesso!")
    end

  end
end
