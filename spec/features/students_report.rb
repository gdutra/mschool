# -*- encoding: utf-8 -*-
require "spec_helper"

describe "Student Report" do

  context "without filter" do
    before do
      visit root_path

      click_link "Relatório"

      click_button "Gerar"
    end

    it "shows the records" do
      page.should have_css('div .group')
    end
  end

  context "filtering" do
     before do
      visit root_path

      click_link "Relatório"

      fill_in "Turma", :with => "2 ano B"

      fill_in "Professor", :with => "Joao"

      click_button "Gerar"
    end

    it "shows the records" do
      page.should have_css('div .group')
    end
  end

end
