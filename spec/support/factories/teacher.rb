# -*- encoding: utf-8 -*-
FactoryGirl.define do
  factory :teacher_1, :class => Teacher do
    name "Marcos"
  end

  factory :teacher_2, :class => Teacher do
    name "Steve"
  end
end
