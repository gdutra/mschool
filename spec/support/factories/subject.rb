# -*- encoding: utf-8 -*-
FactoryGirl.define do
  factory :subject_1, :class => Subject do
    name "Matemática"
  end

  factory :subject_2, :class => Subject do
    name "Inglês"
  end
end
