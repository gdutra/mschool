# -*- encoding: utf-8 -*-
FactoryGirl.define do
  factory :student do
    name "João"
    registration 2222222
  end
end
