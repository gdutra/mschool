class CreateSubjects < ActiveRecord::Migration
  def change
    create_table :subjects do |t|
      t.string :name
      t.belongs_to :group, :null => false
      t.belongs_to :teacher, :null => false
      t.timestamps
    end

    add_index :subjects, :group_id
    add_index :subjects, :teacher_id

    add_foreign_key :subjects, :groups, :dependent => :delete
    add_foreign_key :subjects, :teachers, :dependent => :delete
  end
end
