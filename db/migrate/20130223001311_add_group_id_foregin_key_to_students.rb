class AddGroupIdForeginKeyToStudents < ActiveRecord::Migration
  def change
    add_foreign_key :students, :groups, :dependent => :delete
  end
end
