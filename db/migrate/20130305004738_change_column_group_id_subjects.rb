class ChangeColumnGroupIdSubjects < ActiveRecord::Migration
  def change
    change_column :subjects, :group_id, :integer, :null => true
    change_column :subjects, :teacher_id, :integer, :null => true
  end
end
